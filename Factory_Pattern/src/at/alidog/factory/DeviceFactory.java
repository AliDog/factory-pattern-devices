package at.alidog.factory;

import at.alidog.devices.Smart_Device;
import at.alidog.devices.Smartphone;
import at.alidog.devices.Smartwatch;
import at.alidog.devices.Tablet;

public class DeviceFactory {
	  //use getShape method to get object of type shape 
	   public Smart_Device getDevice(String DeviceType){
	      if(DeviceType == null){
	         return null;
	      }		
	      if(DeviceType.equalsIgnoreCase("Tablet")){
	         return new Tablet();
	         
	      } else if(DeviceType.equalsIgnoreCase("Smartphone")){
	         return new Smartphone();
	         
	      } else if(DeviceType.equalsIgnoreCase("Smartwatch")){
	         return new Smartwatch();
	      }
	      
	      return null;
	   }
}
