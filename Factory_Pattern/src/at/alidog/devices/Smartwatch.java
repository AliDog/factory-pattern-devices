package at.alidog.devices;

import java.util.Random;

public class Smartwatch implements Smart_Device {

	@Override
	public void create() {
		Random randomSize = new Random();
		int low = 1;
		int high = 3;
		int screenSize = randomSize.nextInt(high-low) + low;
		
		System.out.println("Created Smartwatch with a screensize of "+ screenSize +" inch");
		
	}

}
