package at.alidog.devices;

import at.alidog.factory.DeviceFactory;

public class Main {

	public static void main(String[] args) {
	      DeviceFactory Factory = new DeviceFactory();

	      
	      Smartphone smartphone = (Smartphone) Factory.getDevice("Smartphone");

	      smartphone.create();

	      
	      Tablet tablet = (Tablet) Factory.getDevice("Tablet");

	      tablet.create();

	      
	      Smartwatch smartwatch = (Smartwatch) Factory.getDevice("Smartwatch");

	      smartwatch.create();

	}

}
