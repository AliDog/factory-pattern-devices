package at.alidog.devices;

import java.util.Random;

public class Tablet implements Smart_Device {

	@Override
	public void create() {
		Random randomSize = new Random();
		int low = 9;
		int high = 13;
		int screenSize = randomSize.nextInt(high-low) + low;
		
		System.out.println("Created Tablet with a screensize of "+ screenSize +" inch");
		
	}

}
