package at.alidog.devices;

import java.util.Random;

public class Smartphone implements Smart_Device {

	@Override
	public void create() {
		Random randomSize = new Random();
		int low = 4;
		int high = 7;
		int screenSize = randomSize.nextInt(high-low) + low;
		
		System.out.println("Created Smartphone with a screensize of "+ screenSize +" inch");
		
	}
	
}
